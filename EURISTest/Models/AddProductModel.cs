﻿using EURIS.Entities;
using System.Collections.Generic;

namespace EURISTest.Models
{
    public class AddProductModel
    {
        public List<Product> Products { get; set; }
        public int IdCatalog { get; set; }
        public IList<int> SelectedProducts { get; set; }
    }
}