﻿using System.Web.Mvc;
using EURIS.Entities;
using EURIS.Service;
using System.Collections.Generic;

namespace EURISTest.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductManager _service = new ProductManager();

        //
        // GET: /Product/

        public ActionResult Index()
        {
            List<Product> products = new List<Product>();

            products = _service.GetProducts();

            return View(products);
        }

        //
        // GET: /Product/Details/5

        public ActionResult Details(int id = 0)
        {
            Product product = _service.Read(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // GET: /Product/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Product/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product)
        {
            if (_service.CodeUsedYet(product.Code))
            {
                ModelState.AddModelError("Code", "Code is yet used.");
            }

            if (ModelState.IsValid)
            {
                ManageResult(_service.Create(product));
                return RedirectToAction("Index");
            }

            return View(product);
        }

        //
        // GET: /Product/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Product product = _service.Read(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Product/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product)
        {
            if (_service.CodeUsedYet(product.Code, product.Id))
            {
                ModelState.AddModelError("Code", "Code is used for an other product.");
            }

            if (ModelState.IsValid)
            {
                ManageResult(_service.Update(product));
                return RedirectToAction("Index");
            }
            return View(product);
        }

        //
        // GET: /Product/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Product product = _service.Read(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Product/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = _service.Read(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            ManageResult(_service.Delete(product));

            return RedirectToAction("Index");
        }
    }
}