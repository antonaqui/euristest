﻿using System.Web.Mvc;

namespace EURISTest.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly string _koClass = "alert alert-danger";
        private readonly string _koIcon = "fa-thumbs-down";
        private readonly string _erroreStrongMessage = "Error!";
        private readonly string _errorMessage = "Something's gone wrong.";

        private readonly string _okClass = "alert alert-success";
        private readonly string _okIcon = "fa-thumbs-up";
        private readonly string _okMessage = "All's gone ok.";
        private readonly string _okeStrongMessage = "Good news!";

        public void ManageResult(bool result)
        {
            if (result)
            {
                ShowOK();
            }
            else {
                ShowKO();
            }
        }

        private void ShowKO()
        {
            TempData["Popup"] = "SHOW";
            TempData["PopupType"] = _koClass;
            TempData["Icon"] = _koIcon;
            TempData["MessageStrong"] = _erroreStrongMessage;
            TempData["Message"] = _errorMessage;
        }

        private void ShowOK()
        {
            TempData["Popup"] = "SHOW";
            TempData["PopupType"] = _okClass;
            TempData["Icon"] = _okIcon;
            TempData["MessageStrong"] = _okeStrongMessage;
            TempData["Message"] = _okMessage;
        }
    }
}
