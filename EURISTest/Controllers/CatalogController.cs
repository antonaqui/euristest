﻿using System.Collections.Generic;
using System.Web.Mvc;
using EURIS.Entities;
using EURISTest.Models;
using EURIS.Service;

namespace EURISTest.Controllers
{
    public class CatalogController : BaseController
    {
        private readonly ICatalogManager _service = new CatalogManager();

        //
        // GET: /Default1/

        public ActionResult Index()
        {
            List<Catalog> catalogs = new List<Catalog>();

            catalogs = _service.GetCatalogs();

            return View(catalogs);
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }
            return View(catalog);
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Catalog catalog)
        {
            if (_service.CodeUsedYet(catalog.Code)) {
                ModelState.AddModelError("Code", "Code is yet used.");
            }

            if (ModelState.IsValid)
            {
                ManageResult(_service.Create(catalog));
                return RedirectToAction("Index");
            }

            return View(catalog);
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }
            return View(catalog);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Catalog catalog)
        {
            if (_service.CodeUsedYet(catalog.Code, catalog.Id))
            {
                ModelState.AddModelError("Code", "Code is yet used for an other catalog.");
            }

            if (ModelState.IsValid)
            {
                ManageResult(_service.Update(catalog));
                return RedirectToAction("Index");
            }
            return View(catalog);
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }

            return View(catalog);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }

            ManageResult(_service.Delete(catalog));

            return RedirectToAction("Index");
        }

        public ActionResult Products(int id = 0)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }
            return View(catalog);
        }

        public ActionResult RemoveProduct(int idCatalog = 0, int idProduct = 0)
        {
            Catalog catalog = _service.Read(idCatalog);

            if (catalog == null)
            {
                return HttpNotFound();
            }

            ManageResult(_service.RemoveProduct(catalog, idProduct));

            return RedirectToAction("Products", new { @id = idCatalog });
        }

        public ActionResult AddProducts(int id = 0)
        {
            Catalog catalog = _service.Read(id);

            if (catalog == null)
            {
                return HttpNotFound();
            }

            AddProductModel pageModel = new AddProductModel();

            pageModel.Products = _service.GetProductNotCoupled(catalog);
            pageModel.IdCatalog = catalog.Id;

            return View(pageModel);
        }

        [HttpPost, ActionName("AddProducts")]
        public ActionResult AddProductsPost(AddProductModel inModel)
        {
            Catalog catalog = _service.Read(inModel.IdCatalog);
            if (catalog == null)
            {
                return HttpNotFound();
            }

            ManageResult(_service.AddProduct(catalog, inModel.SelectedProducts));

            return RedirectToAction("Products", new { id = inModel.IdCatalog });
        }
    }
}