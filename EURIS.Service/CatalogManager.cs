﻿using System.Collections.Generic;
using System.Linq;
using EURIS.Entities;
using System.Data;
using System.Transactions;
using System;

namespace EURIS.Service
{
    public class CatalogManager : ICatalogManager
    {
        private readonly LocalDbEntities context = new LocalDbEntities();

        public bool Create(Catalog entity)
        {
            bool inserted = false;
            try
            {
                context.Catalog.Add(entity);
                context.SaveChanges();
                inserted = true;
            }
            catch
            {
                inserted = false;
            }

            return inserted;
        }

        public Catalog Read(int idEntity)
        {
            Catalog catalog = new Catalog();
            catalog = context.Catalog.Find(idEntity);
            return catalog;
        }

        public bool Update(Catalog entity)
        {
            bool updated = false;
            try
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                updated = true;
            }
            catch
            {
                updated = false;
            }

            return updated;
        }

        public bool Delete(Catalog entity)
        {
            bool deleted = false;
            try
            {
                List<Product> products = entity.Products.ToList();

                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    foreach (Product product in products)
                    {
                        entity.Products.Remove(product);
                    }

                    context.Catalog.Remove(entity);
                    context.SaveChanges();
                    scope.Complete();
                }
                deleted = true;
            }
            catch
            {
                deleted = false;
            }

            return deleted;
        }

        public List<Catalog> GetCatalogs()
        {
            List<Catalog> catalogs = new List<Catalog>();

            catalogs = (from item in context.Catalog
                        select item).ToList();

            return catalogs;
        }

        public bool AddProduct(Catalog catalog, IList<int> idsProduct)
        {
            bool coupled = false;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    foreach (int i in idsProduct)
                    {
                        Product product = context.Product.Find(i);
                        catalog.Products.Add(product);
                    }
                    context.SaveChanges();
                    scope.Complete();
                }
                coupled = true;
            }
            catch
            {
                coupled = false;
            }

            return coupled;
        }

        public List<Product> GetProductNotCoupled(Catalog catalog)
        {
            List<Product> productsNotCoupled = new List<Product>();

            int[] productIds = catalog.Products.Select(x => x.Id).ToArray();
            productsNotCoupled = context.Product.Where(x => !productIds.Contains(x.Id)).ToList();

            return productsNotCoupled;
        }

        public bool RemoveProduct(Catalog catalog, int idProduct)
        {
            bool removed = false;
            try
            {
                Product productToRemove = catalog.Products.Where(x => x.Id == idProduct).First();
                catalog.Products.Remove(productToRemove);
                context.SaveChanges();

                removed = true;
            }
            catch
            {
                removed = false;
            }
            return removed;
        }

        public bool CodeUsedYet(string code, int idToExclude = 0)
        {
            bool used = false;

            if (idToExclude != 0)
            {
                used = context.Catalog.Any(m => m.Code.Equals(code) && m.Id != idToExclude);
            }
            else
            {
                used = context.Catalog.Any(m => m.Code.Equals(code));
            }

            return used;
        }
    }
}
