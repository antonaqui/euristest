﻿namespace EURIS.Service
{
    public interface ICRUD<T> where T : class
    {
        /// <summary>
        /// Create an entity
        /// </summary>
        /// <param name="entity">The entity to create</param>
        /// <returns>True is action gone ok, false in other case</returns>
        bool Create(T entity);
        
        /// <summary>
        /// Retrieve a entity by own id
        /// </summary>
        /// <param name="idEntity">The id of entity to retrieve</param>
        /// <returns>True is action gone ok, false in other case</returns>
        T Read(int idEntity);
        
        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity">The entity to update</param>
        /// <returns>True is action gone ok, false in other case</returns>
        bool Update(T entity);
        
        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="entity">The entity to update</param>
        /// <returns>True is action gone ok, false in other case</returns>
        bool Delete(T entity);
    }
}
