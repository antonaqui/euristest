﻿using EURIS.Entities;
using System.Collections.Generic;

namespace EURIS.Service
{
    public interface IProductManager : ICRUD<Product>
    {

        /// <summary>
        /// List all product stored
        /// </summary>
        /// <returns>A list of products</returns>
        List<Product> GetProducts();

        /// <summary>
        /// Verify if code is yet used.
        /// </summary>
        /// <param name="Code">Code to check</param>
        /// <param name="IdToExclude">[optional]Product to esclude</param>
        /// <returns>True is code is used, false the rest of case</returns>
        bool CodeUsedYet(string Code, int IdToExclude = 0);
    }
}
