﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EURIS.Entities;
using System.Data.Entity;
using System.Data;
using System.Transactions;

namespace EURIS.Service
{
    public class ProductManager : IProductManager
    {
        private readonly LocalDbEntities context = new LocalDbEntities();

        public bool Create(Product entity)
        {
            bool inserted = false;
            try
            {
                context.Product.Add(entity);
                context.SaveChanges();
                inserted = true;
            }
            catch
            {
                inserted = false;
            }

            return inserted;
        }

        public Product Read(int idEntity)
        {
            Product product = new Product();
            product = context.Product.Find(idEntity);
            return product;
        }

        public bool Update(Product entity)
        {
            bool updated = false;
            try
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                updated = true;
            }
            catch
            {
                updated = false;
            }

            return updated;
        }

        public bool Delete(Product entity)
        {
            bool deleted = false;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var catalogs = context.Catalog.Where(c => c.Products.Any(p => p.Id == entity.Id));

                    foreach (var catalog in catalogs)
                    {
                        catalog.Products.Remove(entity);
                    }

                    context.Product.Remove(entity);
                    context.SaveChanges();
                    scope.Complete();
                }
                deleted = true;
            }
            catch
            {
                deleted = false;
            }

            return deleted;
        }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();

            products = (from item in context.Product
                        select item).ToList();

            return products;
        }

        public bool CodeUsedYet(string code, int idToExclude = 0)
        {
            bool used = false;

            if (idToExclude != 0)
            {
                used = context.Product.Any(m => m.Code.Equals(code) && m.Id != idToExclude);
            }
            else
            {
                used = context.Product.Any(m => m.Code.Equals(code));
            }

            return used;
        }
    }
}
