﻿using EURIS.Entities;
using System.Collections.Generic;

namespace EURIS.Service
{
    public interface ICatalogManager : ICRUD<Catalog>
    {
        /// <summary>
        /// List all catalogs
        /// </summary>
        /// <returns>All catalogs of system</returns>
        List<Catalog> GetCatalogs();

        /// <summary>
        /// Add a product to catalog
        /// </summary>
        /// <param name="catalog">Catalog interested</param>
        /// <param name="idsProduct">ids of product(s) to add</param>
        /// <returns>True is action gone ok, false in other case</returns>
        bool AddProduct(Catalog catalog, IList<int> idsProduct);

        /// <summary>
        /// Remove a protuct from catalog
        /// </summary>
        /// <param name="catalog">Catalog interested</param>
        /// <param name="idProduct">Id of product to uncouple</param>
        /// <returns>True is action gone ok, false in other case</returns>
        bool RemoveProduct(Catalog catalog, int idProduct);

        /// <summary>
        /// List all products coupled
        /// </summary>
        /// <param name="catalog">Catalog to read</param>
        /// <returns>List of products not coupled</returns>
        List<Product> GetProductNotCoupled(Catalog catalog);

        /// <summary>
        /// Verify if code is yet used.
        /// </summary>
        /// <param name="Code">Code to check</param>
        /// <param name="IdToExclude">[optional]Catalog to esclude</param>
        /// <returns>True is code is used, false the rest of case</returns>
        bool CodeUsedYet(string Code, int IdToExclude = 0);
    }
}
